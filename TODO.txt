======CURRENT======
- Clean-up code

 ======NEXT======
 - Clean-up code


 ======FINISHED======
 - Fix time slider bug
 - Fix volume slider bug
 - fix canvas so it isn't offset by controls at top
 - change colour to only b and w
 - Add file opener
 - Add button to change modes
 - Add wave mode
 - Update time text
