/* NOTE:
    Needs to be used with a server otherwise chrome gives cross-origin
    error for trying to access local file, FF gives a similar error.

    Can use python http.server to run, e.g.
    (root dor of project) python3 -m http.server */

var audioFile, fft, filePath, updatedInfo, modes;

function preload(){
    fileName = "PITCH PERFECT 3 - RIFF OFF [Full Scene] HD 1080p";
    ext = ".mp3"
    filePath = "../audio_files/"
    audioFile = loadSound(filePath + fileName + ext);
    fft = new p5.FFT()
}

function setup(){
    createCanvas(window.innerWidth, window.innerHeight);
    background(0);

    // stops file beig plaeyd multiple times if played while already
    // playing
    audioFile.playMode('untilDone');

    modes = {"0": waves, "1": circle_bars, "2": circle_waves, "3": bars, "4": waves};


    // audioFile.play()
    // audioFile.setVolume(0.01);
    // noLoop();

}

function draw(){
    background(0);

    // checks if audioFile as loaded
    if(!audioFile.isLoaded()) return 0;

    updateSlider();

    if(!updatedInfo) updateAudioInfo();

     let spectrum = fft.analyze();
     let mode = parseInt($("#modes").val());

     // push/pop to stop translate in one funtion interfering with translate in another function
     push();
     modes[mode](spectrum);
     pop();

     if(audioFile.isPlaying()){
         let time = audioFile.currentTime();
         let mins_secs = toMinutes(time);

         $("#cur_time").text(mins_secs);
     }
}

function updateAudioInfo(){
    $("#title").text(fileName);

    // set slider range to song time
    $("#timeSlider").attr("max", audioFile.duration());
    $("#timeSlider").val(0);

    // update time text
    let dur = audioFile.duration();
    let time = toMinutes(dur);

    $("#tot_time").text(time);

    updateVolume();

    updatedInfo = 1;
}

function fileChange(){
    /* changes audio file to one selected by user, has to-be
       in "audio_files" directory */
    let name = $("#fileInput").val();

    // cancelled input
    if(!name) return 0;

    // remove "C:/fake_path" prefix
    let prefix = "C:\\fakepath\\";
    fileName = name.replace(prefix, "");

    // update p5.js SoundFile object
    audioFile.stop(0);

    audioFile = loadSound(filePath + fileName);

    updatedInfo = 0;
}

function circle_waves(spectrum){
    /* visual audio with waves arounnd circle */
    translate(width / 2, height / 2);
    angleMode(DEGREES);
    // noFill();
    // strokeWeight(4);
    // strokeCap(ROUND);
    // strokeJoin(ROUND);
    // stroke(255);
    curveTightness(0);

    // fill(255);
    noStroke();

    let radius = 200;


    fill("#f1737f");
    beginShape();
    for(degree = 0; degree <= 360; degree += 1){
        radius = map(spectrum[degree], 0, 255, 200, 300);
        let x = radius * cos(degree - 90);
        let y = radius * sin(degree - 90);
        curveVertex(x, y);
    }
    endShape(CLOSE)

    fill("#000");
    beginShape();
    for(degree = 360; degree <= 720; degree += 1){
        radius = map(spectrum[degree], 0, 255, 100, 200);
        let x = radius * cos(degree - 90);
        let y = radius * sin(degree - 90);
        curveVertex(x, y);
    }
    endShape(CLOSE)
}

function circle_bars(spectrum){
    translate(width / 2, height / 2);
    fill("#f1737f");
    noStroke();
    // noFill();
    // stroke(255);
    // strokeWeight(2);
    angleMode(DEGREES);
    rectMode(CORNERS);

    let radius = 200;

    let circles = [200, 100];
    let starts = [0, 360];
    let ends = [360, 720]

    for(let i = 0; i < circles.length; i++){
        let radius = circles[i];
        for(degree = starts[i]; degree < ends[i]; degree += 3){
            push();
            let height = map(spectrum[degree], 0, 255, 4, 100);
            let x = radius * cos(degree - 90);
            let y = radius * sin(degree - 90);
            translate(x, y);
            rotate(degree);
            rect(0, 0, 5, -height);
            pop()
        }
    }
}

function circle_pulses(spectrum){

}

function bars(spectrum){
    translate(10, height / 2);
    noStroke()
    rectMode(CENTER);

    fill("#f1737f");

    bar_width = 5;
    bar_spacing = 5;
    bar_total = bar_width + bar_spacing;

    margins = 20;

    no_bars = (bar_total * (width / bar_total)) - margins;

    for(let x = 0, i = 0; x < no_bars; x += bar_total, i += 2){
        let h = map(spectrum[i], 0, 255, 5, 400);
        rect(x, 0, bar_width, -h);
    }
}

function waves(spectrum){
    translate(10, height / 2);
    stroke("#f1737f");
    strokeWeight(2);
    fill("#f1737f");

    let margins = 20;
    let wave_size = 5;

    let total_vertices = (wave_size * (width / wave_size)) - margins;

    beginShape();
    // TOP WAVE
    for(var x = 0, i = 0; x < total_vertices; x += wave_size, i += 2){
        let h = map(spectrum[i], 0, 255, 0, 200) * 1;

        curveVertex(x, -h);
    }

    // BOTTOM WAVE - goes from right to left instead of starting new shape
    //               so can fill wave with colour
    for(x = x, i = i; x >= 0; x -= wave_size, i -= 2){
        let h = map(spectrum[i], 0, 255, 0, 200) * -1;

        curveVertex(x, -h);
    }
    endShape();
}

function triangles(){

}

function updateVolume(){
    let v = parseFloat($("#volumeSlider").val());
    audioFile.setVolume(v);
}

function updateSlider(){
    let slider = $("#timeSlider");

    if(!slider.is(":active") && audioFile.isPlaying()){
        $("#timeSlider").val(audioFile.currentTime());
    }
}

function timeSliderInput(){
    let val = parseFloat($("#timeSlider").val());
    let playing = audioFile.isPlaying();

    audioFile.jump(val);
}

function toMinutes(seconds){
    /* converts seconds to minutes */
    let mins = String((seconds - (seconds % 60)) / 60);
    let secs = String(parseInt(seconds - (mins * 60)));

    // padding to seconds < 10
    if(secs.length < 2) secs = "0" + secs;

    return mins + ":" + secs;
}

// function keyReleased(key){
//     if(key.code == "Space") audioFile.isPlaying() ? audioFile.pause() : audioFile.play();
// }
