# Music
This program visualizes music using Js and the p5.js library.

Works best with Chrome.

## Modes
### Bars
![Alt text](ModeExamples/bars.png?raw=true "Bars")
### Circle Bars
![Alt text](ModeExamples/circle_bars.png?raw=true "Circle Bars")
### Circle Waves
![Alt text](ModeExamples/circle_waves.png?raw=true "Circle Waves")
### Waves
![Alt text](ModeExamples/waves.png?raw=true "Waves")


## Install
```
wget https://gitlab.com/murster972/Music-Visualization/-/archive/master/Music-Visualization-master.zip
cd Music-Visualization-master
mkdir audio_files
```

## Music Files
To open music files on the webpage they need to be placed in the "audio_files" directory

## Running
As it's accessing local files errors are shown when running locally, so needs
to be run using a server. An example of this is shown below, using the python3
http.server.
```
cd Music-Visualization-master
python3 -m http.server
```
After the servers running go-to the address of the server in the browser.
